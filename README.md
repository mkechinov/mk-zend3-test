# MK Test Application

## Specification

 * Вывод данных из таблицы test_tbl (id int(11), name varchar(100))
 * Вывести запись по коду id
 * Разработать модель данных
 * Использовать Factory
 * Использовать Doctrina

## Installation

Clone repository
```bash
$ git clone ...
```

Composer install in app folder
```bash
$ composer install
```

будем использовать MySql

```SQL
CREATE USER 'mk-test'@'localhost' IDENTIFIED BY 'mk-test';
CREATE DATABASE mk_test;
GRANT ALL ON mk_test.* TO 'mk-test'@'localhost';
```

Тут лучше было использовать doctrine migration и fixtures. Но в требованиях не было, а с учетом  нехватки времени быстро создадим таблицу и начальные записи прямым sql.
```SQL
CREATE TABLE `test_tbl` (
  `id` int(11) PRIMARY KEY AUTO_INCREMENT,
  `name` varchar(100) NOT NULL
);

INSERT INTO test_tbl(`name`) VALUES('First test name');
INSERT INTO test_tbl(`name`) VALUES('Second test name');
INSERT INTO test_tbl(`name`) VALUES('Another one');
INSERT INTO test_tbl(`name`) VALUES('Bla bla bla');
INSERT INTO test_tbl(`name`) VALUES('Test item 001');
INSERT INTO test_tbl(`name`) VALUES('Test item 002');
INSERT INTO test_tbl(`name`) VALUES('Test item 003');
INSERT INTO test_tbl(`name`) VALUES('Test item 005');
INSERT INTO test_tbl(`name`) VALUES('Test item 006');
INSERT INTO test_tbl(`name`) VALUES('Test item 007');
INSERT INTO test_tbl(`name`) VALUES('Test item 008');
```

Запускаем в папке проекта:
```
 php -S 0.0.0.0:8080 -t public public/index.php
 ```