<?php
namespace Application\Repository;

use Doctrine\ORM\EntityRepository;
use Application\Entity\Test;

/**
 * This is the custom repository class for Test entity.
 */
class TestRepository extends EntityRepository
{
    /**
     * Retrieves all items in descending id order.
     * @return Query
     */
    public function findTests()
    {
        $entityManager = $this->getEntityManager();

        $queryBuilder = $entityManager->createQueryBuilder();

        $queryBuilder->select('t')
            ->from(Test::class, 't')
            ->orderBy('t.id', 'DESC');

        return $queryBuilder->getQuery();
    }

}