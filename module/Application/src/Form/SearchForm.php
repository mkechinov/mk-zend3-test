<?php

namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;

/**
 * This form is used to search by id
 */
class SearchForm extends Form
{
    /**
     * Constructor.
     */
    public function __construct()
    {
        // Define form name
        parent::__construct('search-form');

        // Set POST method for this form
        $this->setAttribute('method', 'post');

        $this->addElements();
    }

    /**
     * This method adds elements to form (input fields and submit button).
     */
    protected function addElements()
    {
        // Add "title" field
        $this->add([
            'type'  => 'number',
            'name' => 'item_id',
            'attributes' => [
                'id' => 'search_id'
            ],
            'options' => [
                'label' => 'item id',
            ],
        ]);

        // Add the submit button
        $this->add([
            'type'  => 'submit',
            'name' => 'submit',
            'attributes' => [
                'value' => 'Search',
                'id' => 'submitbutton',
            ],
        ]);
    }

}

