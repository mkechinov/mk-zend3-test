<?php
namespace Application\Service;
use Zend\ServiceManager\ServiceManager;
use Application\Entity\Test;
use Zend\Filter\StaticFilter;

/**
 * The ItemManager service is responsible for adding new items
 */
class ItemManager
{
    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager;
     */
    private $entityManager;
    
    /**
     * Constructor.
     */
    public function __construct($entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    /**
     * This method adds a new test item.
     */
    public function addNewTestItem($data)
    {
        // Create new Post entity.
        $post = new Test();
        $post->setName($data['name']);

        // Add the entity to entity manager.
        $this->entityManager->persist($post);
        
        // Apply changes to database.
        $this->entityManager->flush();
    }
}



