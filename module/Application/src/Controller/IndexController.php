<?php

namespace Application\Controller;

use Application\Form\SearchForm;
use Application\Form\TestForm;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use DoctrineORMModule\Paginator\Adapter\DoctrinePaginator as DoctrineAdapter;
use Doctrine\ORM\Tools\Pagination\Paginator as ORMPaginator;
use Zend\Paginator\Paginator;
use Application\Entity\Test;

class IndexController extends AbstractActionController
{

    /**
     * Entity manager.
     * @var Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * Item manager.
     * @var Application\Service\ItemManager
     */
    private $itemManager;

    /**
     * Constructor is used for injecting dependencies into the controller.
     */
    public function __construct($entityManager, $itemManager)
    {
        $this->entityManager = $entityManager;
        $this->itemManager = $itemManager;
    }

    /**
     * This is the default "index" action of the controller.
     * Item list with search form
     */
    public function indexAction()
    {
        // Create the form.
        $form = new SearchForm();

        // This check submit from SearchForm
        $this->checkSearchFormRequest($form);

        $page = $this->params()->fromQuery('page', 1);

        // Get recent posts
        $query = $this->entityManager->getRepository(Test::class)
            ->findTests();

        $adapter = new DoctrineAdapter(new ORMPaginator($query, false));
        $paginator = new Paginator($adapter);
        $paginator->setDefaultItemCountPerPage(10);
        $paginator->setCurrentPageNumber($page);

        // Render the view template.
        return new ViewModel([
            'tests' => $paginator,
            'form' => $form
        ]);
    }

    /**
     * This action displays the "View Item" page
     */
    public function viewAction()
    {
        $itemId = (int)$this->params()->fromRoute('id', -1);

        // Validate input parameter
        if ($itemId < 0) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        // Find the post by ID
        $item = $this->entityManager->getRepository(Test::class)
            ->findOneById($itemId);

        if ($item == null) {
            $this->getResponse()->setStatusCode(404);
            return;
        }

        // Render the view template.
        return new ViewModel([
            'item' => $item
        ]);
    }

    /**
     * This action displays the "New Item" page. The page contains a form.
     */
    public function addAction()
    {
        // Create the form.
        $form = new TestForm();

        // Check whether this post is a POST request.
        if ($this->getRequest()->isPost()) {

            // Get POST data.
            $data = $this->params()->fromPost();

            // Fill form with data.
            $form->setData($data);
            if ($form->isValid()) {

                // Get validated form data.
                $data = $form->getData();

                // Use post manager service to add new post to database.
                $this->itemManager->addNewTestItem($data);

                // Redirect the user to "index" page.
                return $this->redirect()->toRoute('application');
            }
        }

        // Render the view template.
        return new ViewModel([
            'form' => $form
        ]);
    }

    /**
     * This action displays the About page.
     */
    public function aboutAction()
    {
        return new ViewModel();
    }

    /**
     * This check submit from SearchForm
     * and redirect, if form was submitted
     */
    private function checkSearchFormRequest($form)
    {
        // Check whether this post is a POST request. (from Search form)
        if ($this->getRequest()->isPost()) {
            // Get POST data.
            $data = $this->params()->fromPost();

            // Fill form with data.
            $form->setData($data);
            if ($form->isValid()) {

                // Get validated form data.
                $data = $form->getData();

                $itemId = $data['item_id'];

                $this->redirect()->toRoute(
                    'items',
                    ['action'=>'view', 'id'=> $itemId]
                );
            }
        }
    }
}
